# Products Filter VueJs :zap:


## Clone Project

```
git clone https://gitlab.com/SafeMood/products-filter-vue
```


## Project setup

```
cd products-filter-vue
```

```
yarn install
```

### copy env.exemple to env.local


```
cp env.exemple env.local
```

### make sure to put the api address and port

```
VUE_APP_ROOT_API="http://127.0.0.1:8000/api"
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```
